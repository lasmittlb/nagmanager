
import Foundation

public class NagManager {
    private var settings: NagSettings

    public init(settings: NagSettings) {
        self.settings = settings
    }

    func reset() {
        nagLastShown = nil
        firstEvent = nil
        totalEvents = 0
        totalNagsShown = 0
        neverAgain = false
    }

    public func shouldShowNagAfterIncrementing() -> Bool {
        incerementEventCount()
        return shouldShowNag()
    }

    public func incerementEventCount() {
        if firstEvent == nil {
            firstEvent = Date()
        }
        totalEvents += 1
    }

    public func shouldShowNag() -> Bool {
        
        guard neverAgain == false else {
            return false
        }
        
        if let minEvents = settings.minEvents, totalEvents < minEvents {
            return false
        }

        if let maxEverNags = settings.maxEverNags, maxEverNags > 0 && totalNagsShown >= maxEverNags {
            return false
        }

        if let minDurationBeforeNag = settings.minDurationBeforeFirstNag, let firstEvent = firstEvent {
            if let minDateBeforeFirstNag = Calendar.current.date(byAdding: minDurationBeforeNag.component, value: minDurationBeforeNag.count, to: firstEvent) {
                if Date() < minDateBeforeFirstNag {
                    return false
                }
            }
        }

        if let minDurationBetweenNags = settings.minDurationBetweenNags, let nagLastShown = nagLastShown {
            if let minDateBeforeNextNag = Calendar.current.date(byAdding: minDurationBetweenNags.component, value: minDurationBetweenNags.count, to: nagLastShown) {
                if Date() < minDateBeforeNextNag {
                    return false
                }
            }
        }

        let dice = Double.random(in: 0...1)
        if dice <= settings.oddsOfShowingNag {
            nagLastShown = Date()
            totalNagsShown += 1
            return true
        }

        return false
    }

    public func neverNagAgain() {
        neverAgain = true
    }

    internal private(set) var nagLastShown: Date? {
        get {
            guard let date = UserDefaults.standard.object(forKey: "Nag.lastShown.\(settings.id)") as? Date else {
                return nil
            }
            return date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Nag.lastShown.\(settings.id)")
        }
    }

    internal private(set) var firstEvent: Date? {
        get {
            guard let date = UserDefaults.standard.object(forKey: "Nag.firstEvent.\(settings.id)") as? Date else {
                return nil
            }
            return date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Nag.firstEvent.\(settings.id)")
        }
    }

    internal private(set) var totalEvents: Int {
        get {
            return UserDefaults.standard.integer(forKey: "Nag.totalEvents.\(settings.id)")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Nag.totalEvents.\(settings.id)")
        }
    }

    internal private(set) var totalNagsShown: Int {
        get {
            return UserDefaults.standard.integer(forKey: "Nag.totalNags.\(settings.id)")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Nag.totalNags.\(settings.id)")
        }
    }
    
    internal private(set) var neverAgain: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "Nag.neverAgain.\(settings.id)")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Nag.neverAgain.\(settings.id)")
        }
    }
}
