import Foundation

/// All conditions must be met, in other words they are effectively ANDed together
public struct NagSettings {

    public struct TimeDuration {
        var component: Calendar.Component
        var count: Int
        
        public init(component: Calendar.Component = .day, count: Int = 1) {
            self.component = component
            self.count = count
        }
    }

    /// Unique identifier
    var id: String

    /// Minimum events that must be recieved before nag can fire
    var minEvents: Int?

    /// Minimum time between showing nags
    var minDurationBetweenNags:TimeDuration? = TimeDuration()

    /// Max number of times this nag can be shown.<=0 means this setting is ignored
    var maxEverNags: Int?

    /// Minimum time before first nag
    var minDurationBeforeFirstNag:TimeDuration? = TimeDuration()

    /// Probability of nagging. Range is 0.0 for never nag to 1.0 for always nag
    var oddsOfShowingNag: Double
    
    public init(id: String, minEvents: Int? = nil, minDurationBetweenNags: TimeDuration? = nil, maxEverNags: Int? = 0, minDurationBeforeFirstNag: TimeDuration? = nil, oddsOfShowingNag: Double = 1) {
        self.id = id
        self.minEvents = minEvents
        self.minDurationBetweenNags = minDurationBetweenNags
        self.maxEverNags = maxEverNags
        self.minDurationBeforeFirstNag = minDurationBeforeFirstNag
        self.oddsOfShowingNag = oddsOfShowingNag
    }

    mutating public func always() {
        minEvents = 0
        minDurationBetweenNags = nil
        maxEverNags = 1000000
        minDurationBeforeFirstNag = nil
        oddsOfShowingNag = 1
    }

    mutating public func never() {
        minEvents = nil
        minDurationBetweenNags = nil
        maxEverNags = nil
        minDurationBeforeFirstNag = nil
        oddsOfShowingNag = 0
    }
}
