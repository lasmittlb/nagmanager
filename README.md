# README #

Decide whether to present a message to a user based on various events over a given period of time.

### Sample Code ###

```
import NagManager


var appAdSettings = NagSettings(id: "main.app.appAd",
                       minDurationBetweenNags: NagSettings.TimeDuration(component: .hour, count: 3),
                       maxEverNags: 3,
                       minDurationBeforeFirstNag: NagSettings.TimeDuration(component: .day, count: 1),
                       oddsOfShowingNag: 0.5)

#if DEBUG
appAdSettings.always()
#endif

let nagger = NagManager(settings: appAdSettings)
if nagger.shouldShowNagAfterIncrementing() {
    // show ad
}
```
