import XCTest

import NagManagerTests

var tests = [XCTestCaseEntry]()
tests += NagManagerTests.allTests()
XCTMain(tests)
