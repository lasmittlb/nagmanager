import XCTest
@testable import NagManager

final class NagManagerTests: XCTestCase {
    func testAlways() {
        var nagSettings = NagSettings(id: "abc")
        nagSettings.always()
        
        let manager = NagManager(settings: nagSettings)
        XCTAssertTrue(manager.shouldShowNagAfterIncrementing())
    }
    
    func testNever() {
        var nagSettings = NagSettings(id: "abc")
        nagSettings.never()
        
        let manager = NagManager(settings: nagSettings)
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
    }
    
    func testCount() {
        let nagSettings = NagSettings(id: "test", minEvents: 2,
                                      minDurationBetweenNags: nil,
                                      maxEverNags: 1,
                                      minDurationBeforeFirstNag: nil,
                                      oddsOfShowingNag: 1)
        
        let manager = NagManager(settings: nagSettings)
        manager.reset()
        
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
        XCTAssertTrue(manager.shouldShowNagAfterIncrementing())
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
        XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
    }

    
    func testNeverAgain() {
        let nagSettings = NagSettings(id: "neverTest",
                                      minEvents: 0,
                                      minDurationBetweenNags: nil,
                                      maxEverNags: 100,
                                      minDurationBeforeFirstNag: nil,
                                      oddsOfShowingNag: 1)
        
        let manager = NagManager(settings: nagSettings)
        manager.reset()

        for _ in 0 ... Int.random(in: 0...50) {
            XCTAssertTrue(manager.shouldShowNagAfterIncrementing())
        }
        
        manager.neverNagAgain()
        
        for _ in 0 ... 100 {
            XCTAssertFalse(manager.shouldShowNagAfterIncrementing())
        }

        manager.reset()
        
        XCTAssertTrue(manager.shouldShowNagAfterIncrementing())
    }

    static var allTests = [
        ("testAlways", testAlways),
        ("testNever", testNever),
        ("testCount", testCount),
    ]
}
